package com.epam.boot_api_rest.entitys;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;


@Data
@AllArgsConstructor
@Builder
@Entity
@Table(name = "gifts")
public class GiftCertificate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private BigDecimal price;


    @Column(name = "duration")
    private int duration;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "last_update")
    private LocalDate lastUpdateDate;


    public GiftCertificate() {

    }

    @ManyToMany
    @JoinTable(
            name = "gifts_tags",
            joinColumns = @JoinColumn(name = "giftCertificate_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    Set<Tag> assignedTags = new HashSet<>();

    public GiftCertificate(String name, String description, BigDecimal price, int duration, LocalDate createDate, LocalDate lastUpdateDate, boolean b) {
        this.name=name;
        this.description = description;
        this.price = price;
        this.duration=duration;
        this.createDate=createDate;
        this.lastUpdateDate=lastUpdateDate;

    }


    public void addTag(Tag tag) {
        this.assignedTags.add(tag);
        tag.getGiftsSets().add(this);
    }


    public void removeTag(Long tagId) {
        Tag tag = this.assignedTags.stream().filter(t -> t.getId() == tagId).findFirst().orElse(null);
        if (tag != null) {
            this.assignedTags.remove(tag);
            tag.getGiftsSets().remove(this);
        }

    }


}
