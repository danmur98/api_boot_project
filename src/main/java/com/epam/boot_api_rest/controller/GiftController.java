package com.epam.boot_api_rest.controller;


import com.epam.boot_api_rest.entitys.GiftCertificate;
import com.epam.boot_api_rest.repository.GiftRepository;
import com.epam.boot_api_rest.repository.TagRepository;
import com.epam.boot_api_rest.service.ServiceGift;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class GiftController {



    @Autowired
    private ServiceGift serviceGift;

    @Autowired
    private GiftRepository giftRepository;

    @Autowired
    private TagRepository tagRepository;



    @GetMapping("/gifts")
    public ResponseEntity<List<GiftCertificate>> getALLGiftCertificates(@RequestParam(required = false) String name) {
        List<GiftCertificate> giftCertificates= new ArrayList<GiftCertificate>();

        return new ResponseEntity<>(giftCertificates, HttpStatus.OK);
    }


    @GetMapping("/gifts/{id}")
    public ResponseEntity<GiftCertificate> getGiftCertificateById(@PathVariable("id") long id) {
        GiftCertificate giftCertificate = giftRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found certificate with id = " + id));

        return new ResponseEntity<>(giftCertificate, HttpStatus.OK);
    }



    @PostMapping("/gifts")
    public ResponseEntity<GiftCertificate> createGiftCertificate(@RequestBody GiftCertificate giftCertificate) {
        GiftCertificate _giftCertificate = giftRepository.save(new GiftCertificate(giftCertificate.getName(),giftCertificate.getDescription(),giftCertificate.getPrice(),giftCertificate.getDuration(),giftCertificate.getCreateDate(),giftCertificate.getLastUpdateDate(),true));
        return new ResponseEntity<>(_giftCertificate, HttpStatus.CREATED);
    }

    @PutMapping("/gifts/{id}")
    public ResponseEntity<GiftCertificate> updateGiftCertificate(@PathVariable("id") long id, @RequestBody GiftCertificate giftCertificate) {
        GiftCertificate _giftCertificate = giftRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found certficate with id = " + id));

        _giftCertificate.setName(giftCertificate.getName());
        _giftCertificate.setDescription(giftCertificate.getDescription());
        _giftCertificate.setPrice(giftCertificate.getPrice());
        _giftCertificate.setDuration(giftCertificate.getDuration());
        _giftCertificate.setCreateDate(giftCertificate.getCreateDate());
        _giftCertificate.setLastUpdateDate(giftCertificate.getLastUpdateDate());

        return new ResponseEntity<>(giftRepository.save(_giftCertificate), HttpStatus.OK);
    }

    @DeleteMapping("/gifts/{id}")
    public ResponseEntity<HttpStatus> deleteGiftCertificate(@PathVariable("id") long id) {
        giftRepository.deleteById(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    /*

    @PostMapping("/gifts")
    public ResponseEntity<GiftCertificate> saveCertificate(@RequestBody GiftCertificate giftCertificate) {
        serviceGift.saveGiftCertificate(giftCertificate);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = {"/gifts", "/{giftId}"})
    public List<GiftCertificate> getCertificate(@PathVariable(required = false) Long giftId) {
        return serviceGift.getGiftCertificates(giftId);
    }

    @DeleteMapping("gifts/{giftId}")
    public ResponseEntity removeCertificate(@PathVariable Long giftId){
        serviceGift.deleteGiftCertificate(giftId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/{giftId}/tags/{tagId}")
    public GiftCertificate assignTagCertificate(
            @PathVariable Long giftId,
            @PathVariable Long tagId
    ){
        return serviceGift.assignTagToCertificate(giftId, tagId);
    }
*/

}
