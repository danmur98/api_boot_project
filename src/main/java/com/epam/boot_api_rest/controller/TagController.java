package com.epam.boot_api_rest.controller;


import com.epam.boot_api_rest.entitys.GiftCertificate;
import com.epam.boot_api_rest.entitys.Tag;
import com.epam.boot_api_rest.repository.GiftRepository;
import com.epam.boot_api_rest.repository.TagRepository;
import com.epam.boot_api_rest.service.ServiceTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class TagController {

/*
    @Autowired
    private ServiceTag serviceTag;
*/


    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private GiftRepository giftRepository;





    @GetMapping("/tags")
    public ResponseEntity<List<Tag>> getAllTags() {
        List<Tag> tags = new ArrayList<Tag>();

        tagRepository.findAll().forEach(tags::add);

        if (tags.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(tags, HttpStatus.OK);
    }

    @GetMapping("/tags/{id}")
    public ResponseEntity<Tag> getTagsById(@PathVariable(value = "id") Long id) {
        Tag tag = tagRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found Tag with id = " + id));

        return new ResponseEntity<>(tag, HttpStatus.OK);
    }

    @GetMapping("/gifts/{giftId}/tags")
    public ResponseEntity<List<Tag>> getAllTagsByGiftCertificateId(@PathVariable(value = "giftId") Long giftId) {
        if (!giftRepository.existsById(giftId)) {
            throw new ResourceNotFoundException("Not found certificate with id = " + giftId);
        }

        List<Tag> tags = tagRepository.findTagsByGiftCertificatesId(giftId);
        return new ResponseEntity<>(tags, HttpStatus.OK);
    }



    @GetMapping("/tags/{tagId}/gifts")
    public ResponseEntity<List<GiftCertificate>> getAllGiftCertificateByTagId(@PathVariable(value = "tagId") Long tagId) {
        if (!tagRepository.existsById(tagId)) {
            throw new ResourceNotFoundException("Not found Tag  with id = " + tagId);
        }

        List<GiftCertificate> tutorials = giftRepository.findGiftCertificatesByTagsId(tagId);
        return new ResponseEntity<>(tutorials, HttpStatus.OK);
    }


    @PostMapping("/gifts/{giftId}/tags")
    public ResponseEntity<Tag> addTag(@PathVariable(value = "giftId") Long giftId, @RequestBody Tag tagRequest) {
        Tag tag = giftRepository.findById(giftId).map(giftCertificate -> {
            long tagId = tagRequest.getId();

            // tag is existed
            if (tagId != 0L) {
                Tag _tag = tagRepository.findById(tagId)
                        .orElseThrow(() -> new ResourceNotFoundException("Not found Tag with id = " + tagId));
                giftCertificate.addTag(_tag);
                giftRepository.save(giftCertificate);
                return _tag;
            }

            // add and create new Tag
            giftCertificate.addTag(tagRequest);
            return tagRepository.save(tagRequest);
        }).orElseThrow(() -> new ResourceNotFoundException("Not found certificate with id = " + giftId));

        return new ResponseEntity<>(tag, HttpStatus.CREATED);
    }

    @PutMapping("/tags/{id}")
    public ResponseEntity<Tag> updateTag(@PathVariable("id") long id, @RequestBody Tag tagRequest) {
        Tag tag = tagRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("TagId " + id + "not found"));

        tag.setName(tagRequest.getName());

        return new ResponseEntity<>(tagRepository.save(tag), HttpStatus.OK);
    }

    @DeleteMapping("/gifts/{giftId}/tags/{tagId}")
    public ResponseEntity<HttpStatus> deleteTagFromGiftCertificate(@PathVariable(value = "giftId") Long giftId, @PathVariable(value = "tagId") Long tagId) {
        GiftCertificate giftCertificate = giftRepository.findById(giftId)
                .orElseThrow(() -> new ResourceNotFoundException("Not found certificate with id = " + giftId));

        giftCertificate.removeTag(tagId);
        giftRepository.save(giftCertificate);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/tags/{id}")
    public ResponseEntity<HttpStatus> deleteTag(@PathVariable("id") long id) {
        tagRepository.deleteById(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


/*
    @PostMapping("/tags")
    public ResponseEntity createTag(@RequestBody Tag tag) {
        serviceTag.saveTag(tag);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping(value = {"/tags", "/{tagId}"})
    public List<Tag> getTags(@PathVariable(required = false) Long tagId) {
        return serviceTag.getTags(tagId);
    }

    @DeleteMapping("/tags/{tagId}")
    public ResponseEntity removeTag(@PathVariable Long tagId) {
        serviceTag.deleteTag(tagId);
        return new ResponseEntity(HttpStatus.OK);
    }

*/

}
