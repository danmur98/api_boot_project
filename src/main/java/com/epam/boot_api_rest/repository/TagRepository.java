package com.epam.boot_api_rest.repository;

import com.epam.boot_api_rest.entitys.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
   // List<Tag> findAllByTagId(long tagId);
    List<Tag> findTagsByGiftCertificatesId(Long gifId);
}
