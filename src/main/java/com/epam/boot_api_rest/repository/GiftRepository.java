package com.epam.boot_api_rest.repository;

import com.epam.boot_api_rest.entitys.GiftCertificate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GiftRepository extends JpaRepository<GiftCertificate, Long> {
  //  List<GiftCertificate> findAllByGiftId(Long giftId);
   List<GiftCertificate> findGiftCertificatesByTagsId(Long tagId);
}
